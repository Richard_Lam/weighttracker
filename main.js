const CALENDAR = document.getElementById("calendar");

const isLeapYear = (year) => {
    return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
}

const getTotalDays = (month, year) => {
    if ([3, 5, 8, 10].includes(month)) {
        return 30
    }
    if (month == 1) {
        if (isLeapYear(year)) {
            return 29;
        }
        return 28;
    }
    return 31
}
const getStartingDayOfWeek = (month, year) => {
    return new Date(year + "-" + (month+1) + "-01").getDay();
}

const generateCalendar = (month, year) => {
    const totalDays = getTotalDays(month, year);
    let currentDayOfWeek = getStartingDayOfWeek(month, year);
    let calendarString = "<tr>" + "<td></td>".repeat(currentDayOfWeek);
    for (let i=1; i <= totalDays; i++) {
        calendarString += `<td onClick="selectDate(${month+1}, ${i}, ${year})">${i}</td>`
        currentDayOfWeek++;
        if (currentDayOfWeek > 6) {
            calendarString += "</tr>";
            calendarString += "<tr>";
            currentDayOfWeek = 0;
        }
    }
    calendarString += "</tr>";
    CALENDAR.innerHTML += calendarString;
}

const selectDate = (month, day, year) => {
    SELECTED_DATE = `${month}-${day}-${year}`;
    console.log("SELECTED: " + SELECTED_DATE);
}

const setWeight = () => {
    const weight = document.getElementById("weight").value;
    console.log(weight);
    WEIGHT_MANAGER.setWeight(SELECTED_DATE, weight);
    console.log("SET: " + SELECTED_DATE + " | WEIGHT: " + weight);
}

let SELECTED_DATE = "";
const WEIGHT_MANAGER = new WeightManager();
let [MONTH, YEAR] = [new Date().getUTCMonth(), new Date().getUTCFullYear()]
generateCalendar(MONTH, YEAR);
