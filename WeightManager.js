class WeightManager {
    setWeight(date, weight) {
        console.log(localStorage.getItem("dates"));
        if (localStorage.getItem("dates") == null) {
            localStorage.setItem("dates", "{}");
        }
        console.log("DATES: " + localStorage.getItem("dates"));
        let dates = JSON.parse(localStorage.getItem("dates"));
        console.log(dates);
        dates[date] = weight;
        localStorage.setItem("dates", JSON.stringify(dates));
    }
    getWeight(date) {
        let dates = JSON.parse(localStorage.getItem("dates"));
        if (date in dates) {
            return dates[date];
        }
        return "";
    }
}
